// 配置路由规则
import VueRouter from 'vue-router'
//默认商品列表
// import BodyBase from '../pages/BodyBase'
//默认商品广告列表
import AdvList from '../pages/AdvList'
import xqye from '../pages/xqye'
import cartList from '../pages/cartList'

//创建并暴露一个路由器
export default new VueRouter({
    routes: [
        // {
        //     path: '/',
        //     component: BodyBase,
        // },
        // {
        //     path: '/show',
        //     component: xqye,
        // },
        {
            name:'guanggao',
            path: '/adv',
            component: AdvList,
    
        },
        {
            name:'xqye',
            //path:'/detail',
            path: '/xqye/:id/:name',
            component: xqye,
            //props:{id:'id1',name:'name1'}, 对象写法
            props:true
        },
        {
            name:'cartList',
            path:'/cartList',
            // path: '/cartList',
            component: cartList,
            //props:{id:'id1',name:'name1'}, 对象写法
            props:true
        },

        // {
        //     name:'detail',
        //     path:'/detail/:id/:name/:price',
        //     //path:'/detail',
        //     component: Detail,
        //     props:true,
        // },
      
    ]
})

// export default router