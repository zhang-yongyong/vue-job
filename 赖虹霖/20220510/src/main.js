import Vue from 'vue'
import App from './App.vue'

//引入VueRouter
import VueRouter from 'vue-router';
import Vuex from 'vuex';
//应用插件
Vue.use(VueRouter);
Vue.use(Vuex);


//引入路由器
import router from './router/index'

const store = new Vuex.Store({
  //状态,就是数据
    state: {
      title:'外卖系统',
      price:100,
      num:66,
      products:[
        {id:0,show: false,name:'苹果笔记本',img:'https://img1.baidu.com/it/u=591631117,2275774035&fm=253&fmt=auto&app=138&f=JPEG?w=667&h=500',kucun:100,num:0,price:3000},
        {id:1,show: false,name:'灰色笔记本',img:'https://img0.baidu.com/it/u=3297317195,345187680&fm=253&fmt=auto&app=120&f=JPEG?w=667&h=500',kucun:100,num:0,price:3000},
        {id:2,show: false,name:'小米笔记本',img:'https://img1.baidu.com/it/u=3413204352,1037999762&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=375',kucun:100,num:0,price:3000},
        {id:3,show: false,name:'苹果笔记本',img:'https://img1.baidu.com/it/u=142621668,3476222106&fm=253&fmt=auto?w=667&h=500',kucun:100,num:0,price:3000}
    ],
  

      },
    })  



//关闭生产环境提交
Vue.config.productionTip = false
//render 渲染 temldate
new Vue({
  el:'#app',
  // template:'<App></App>',
  // components:{
  //   App,
  // }
  //脚手架引入的vue是残缺的，为了性能
  render: (createElement) => {
    // console.log(typeof createElement);
    return createElement(App)
   },
   router,
   store,
   beforeCreate:function(){
    // new Vue.extend({})//创建一个组件事件，组件实例中的功能 vm 都有，所以  Vue.prototype.public = new  Vue.extend({}) 可以用 this 替换
    Vue.prototype.public = this
    Vue.prototype.$bus = this//推荐的写法，bus 就是总线的意思，加 $ 为了跟vue 属性的命名一致
  }
})
