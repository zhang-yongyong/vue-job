## 什么是axios

Axios是一个基于Promise 用于浏览器和 nodejs 的 HTTP 客户端，本质上也是对原生XHR的封装，只不过它是Promise的实现版本，符合最新的ES规范。

关于axios的特点，可以查看axios中文文档，还可以了解一下axios, ajax和fetch的详细比较内容。

接下来开始安装axios，在项目目录下执行npm i axios vue-axios（不同的node版本，成功后的提示不同）

## 为什么不用jquery？

## 具体的安装与使用
 
### 安装vue-axios
  npm i install vue-axios

### 引入第三方组件
  import Vue from "vue";
  import axios from "axios";
  import VueAxios from "vue-axios";
 
  Vue.use(VueAxios, axios) 

### 具体请求

  get 请求
  this.axios.get(url).then(res => {
      //请求成功，触发then中的函数
      console.log(res)  
    })
     .catch(error =>
      //请求失败，触发catch中的函数 可省略
      console.log(error)
    )
   post 请求
   let data = { account: this.account, password: this.pwd };

   his.$axios
         .post("/user/login/", data)
         .then(res => {
           
         }) 