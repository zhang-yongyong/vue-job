# vue-vip/仿唯品会商城app

#### 介绍
仿唯品会移动端项目

#### 软件架构
软件架构说明-使用vue/vue-router/axios/vant/css等技术。


#### 项目说明

1.  该项目包含首页，购物车以及个人中心模块。
2.  实现了下拉加载更多/加入购物车/购物车加减/个人中心头像和昵称修改/收货地址增加/删除/修改等功能。

#### 项目地址：

1.  https://gitee.com/zhang-huili/vue-vip.git。
