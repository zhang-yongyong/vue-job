// 配置路由规则
import VueRouter from 'vue-router'
//默认商品列表
import BodyBase from '../pages/BodyBase'
//默认商品广告列表
import AdvList from '../pages/AdvList'
import Detail from '../pages/Detail'

//创建并暴露一个路由器
export default new VueRouter({
    routes: [
        {
            path: '/',
            component: BodyBase,
        },
        {
            path: '/base',
            component: BodyBase,
        },
        {
            name:'guanggao',
            path: '/adv',
            component: AdvList,
            children:[
                {
                    path: 'show',
                    component: Detail,
                    children:[
                        {
                            name:'gson',
                            path: 'detail',
                            component: Detail,
                        }
                    ]
                }
            ]
        },
        {
            name:'detail',
            path:'/detail/:id/:name/:price',
            //path:'/detail',
            component: Detail,
            props:true,
        },
      
    ]
})

// export default router