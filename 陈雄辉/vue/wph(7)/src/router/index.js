import VueRouter from 'vue-router'
import GuoJi from '../item/GuoJi.vue'
import ThreeFold from '../item/ThreeFold'
import weipin from '../item/WeiPin'
import CarList from '../item/CarList'
import baskball from '../detail/baskball'
import shorts from '../detail/shorts'
import shose from '../detail/shose'
import tshirt from '../detail/tshirt'
import siwa from '../detail/siwa'
import GuiDe from '../item/GuiDe';
export default new VueRouter({
    routes:[
        {path:'/GuoJi',component:GuoJi},
        {path:'/ThreeFold',component:ThreeFold},
        {path:'/WeiPin',component:weipin},
        {path:'/CarList',component:CarList,name:'cartList', props:true},
        {path:'/backball',component:baskball,name:'BaskBall', props:true},
        {path:'/shorts',component:shorts,name:'ShortS', props:true},
        {path:'/shose',component:shose,name:'ShoSe', props:true},
        {path:'/tshirt',component:tshirt,name:'TShirt', props:true},
        {path:'/siwa',component:siwa,name:'SiWa', props:true},
        {path:'/GuiDe',component:GuiDe,name:'GuiDe',props:true}


    ]
})