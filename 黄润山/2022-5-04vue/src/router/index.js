// 配置路由规则
import VueRouter from 'vue-router'
//默认的商品首页
import BodyBase from '../pages/BodyBase'
//活动列表页
import AdvList from '../pages/AdvList'
//活动商品详情页
import Detail from '../pages/Detail'
import CartList from '../pages/CartList'

//创建并暴露一个路由器
export default new VueRouter({
    routes: [
        {
            path: '/',
            component:BodyBase,
        },
        {
            path: '/base',
            component: BodyBase,
        },
        {
            path:'/adv',
            component:AdvList
        },
        {
            name:'detail',
            path:'/detail/:id/:name',
            component:Detail,
            props:true
        },
        {
            name:'cartList',
            path:'/cartList',
            component:CartList,
            props:true
        }
    ]
})
