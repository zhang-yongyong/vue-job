const user={
    namespaced: true,
    //每个模块都有自己的 state mutation ....
    state:{
      userInfo:{name:'刘斌明天要准备',age:29}
    },
    mutations:{
      changeUser(state,payload){
        state.userInfo=payload;
        
      }
    },
    actions:{
      changeUserByAction({commit},payload){
           commit('changeUser',payload)
           //触发全局的mutation的修改
           commit('makeMoney',1000,{ root: true })
      }
    }
    
  }
  export default user;